/*   
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.musicplayer;

import tr.com.cty.ListActivity;
import tr.com.cty.PlayerState;
import tr.com.cty.musiccloud.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;

/**
 * Main activity: shows media player buttons. This activity shows the media
 * player buttons and lets the user click them. No media handling is done here
 * -- everything is done by passing Intents to our {@link MusicService}.
 * */
public class MainActivity extends Activity implements OnClickListener,
		OnGestureListener {
	private static final int MAJOR_MOVE = 120;

	/**
	 * The URL we suggest as default when adding by URL. This is just so that
	 * the user doesn't have to find an URL to test this sample.
	 */
	final String SUGGESTED_URL = "http://www.vorbis.com/music/Epoq-Lepidoptera.ogg";

	Button mPlayButton;
	Button mPauseButton;
	Button mSkipButton;
	Button mRewindButton;
	Button mStopButton;
	Button mEjectButton;
	Button mShuffleButton;
	Button mPlaylistButton;

	private GestureDetector gestureDetector;

	private OnTouchListener gestureListener;

	/**
	 * Called when the activity is first created. Here, we simply set the event
	 * listeners and start the background service ({@link MusicService}) that
	 * will handle the actual media playback.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		mPlayButton = (Button) findViewById(R.id.playbutton);
		mPauseButton = (Button) findViewById(R.id.pausebutton);
		mSkipButton = (Button) findViewById(R.id.skipbutton);
		mRewindButton = (Button) findViewById(R.id.rewindbutton);
		mStopButton = (Button) findViewById(R.id.stopbutton);
		mEjectButton = (Button) findViewById(R.id.ejectbutton);
		mShuffleButton = (Button) findViewById(R.id.shufflebutton);
		mPlaylistButton = (Button) findViewById(R.id.playlistbutton);

		mPlayButton.setOnClickListener(this);
		mPauseButton.setOnClickListener(this);
		mSkipButton.setOnClickListener(this);
		mRewindButton.setOnClickListener(this);
		mStopButton.setOnClickListener(this);
		mEjectButton.setOnClickListener(this);
		mShuffleButton.setOnClickListener(this);
		mPlaylistButton.setOnClickListener(this);

		gestureDetector = new GestureDetector(getApplicationContext(), this);

	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		return gestureDetector.onTouchEvent(event);
	}

	public void onClick(View target) {
		// Send the correct intent to the MusicService, according to the button
		// that was clicked
		if (target == mPlayButton)
			startService(new Intent(MusicService.ACTION_PLAY));
		else if (target == mPauseButton)
			startService(new Intent(MusicService.ACTION_PAUSE));
		else if (target == mSkipButton)
			startService(new Intent(MusicService.ACTION_SKIP));
		else if (target == mRewindButton)
			startService(new Intent(MusicService.ACTION_REWIND));
		else if (target == mStopButton)
			startService(new Intent(MusicService.ACTION_STOP));
		else if (target == mEjectButton) {
			showUrlDialog();
		} else if (target == mShuffleButton) {
			if (PlayerState.getInstance().getCurrentMode() == PlayerState.Mode.SHUFFLE) {
				mShuffleButton.setBackgroundResource(R.drawable.shuffle_off);
				PlayerState.getInstance()
						.setCurrentMode(PlayerState.Mode.QUEUE);
			} else {
				mShuffleButton.setBackgroundResource(R.drawable.shuffle_on);
				PlayerState.getInstance().setCurrentMode(
						PlayerState.Mode.SHUFFLE);
			}
		} else if (target == mPlaylistButton) {
			startActivity(new Intent(this, ListActivity.class));
		}

	}

	/**
	 * Shows an alert dialog where the user can input a URL. After showing the
	 * dialog, if the user confirms, sends the appropriate intent to the
	 * {@link MusicService} to cause that URL to be played.
	 */
	void showUrlDialog() {
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
		alertBuilder.setTitle("Manual Input");
		alertBuilder.setMessage("Enter a URL (must be http://)");
		final EditText input = new EditText(this);
		alertBuilder.setView(input);

		input.setText(SUGGESTED_URL);

		alertBuilder.setPositiveButton("Play!",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dlg, int whichButton) {
						// Send an intent with the URL of the song to play. This
						// is expected by
						// MusicService.
						Intent i = new Intent(MusicService.ACTION_URL);
						Uri uri = Uri.parse(input.getText().toString());
						i.setData(uri);
						startService(i);
					}
				});
		alertBuilder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dlg, int whichButton) {
					}
				});

		alertBuilder.show();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
		case KeyEvent.KEYCODE_HEADSETHOOK:
			startService(new Intent(MusicService.ACTION_TOGGLE_PLAYBACK));
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.listallsongs:
			Intent intent = new Intent(this, ListActivity.class);
			PlayerState.getInstance().setCurrentPlayList(
					MusicRetriever.ALL_SONGS);
			startActivity(intent);
			return true;
		case R.id.listfavorites:
			Intent intent2 = new Intent(this, ListActivity.class);
			PlayerState.getInstance().setCurrentPlayList(
					MusicRetriever.FAVORITES);
			startActivity(intent2);
			return true;
		case R.id.menu_settings:
			// startActivity(new Intent(this, Help.class));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onDown(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		int dx = (int) (e2.getX() - e1.getX());
		// don't accept the fling if it's too short
		// as it may conflict with a button push
		if (Math.abs(dx) > MAJOR_MOVE
				&& Math.abs(velocityX) > Math.abs(velocityY)) {
			if (velocityX > 0) {
				startActivity(new Intent(this, ListActivity.class));
				overridePendingTransition(R.anim.slide_right, R.anim.hold);
			} else {
				startActivity(new Intent(this, ListActivity.class));
				overridePendingTransition(R.anim.slide_left, R.anim.hold);
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		// TODO Auto-generated method stub
		return false;
	}

}
