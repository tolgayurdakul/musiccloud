/*   
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.musicplayer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import tr.com.cty.PlayList;
import tr.com.cty.PlayerState;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

/**
 * Retrieves and organizes media to play. Before being used, you must call
 * {@link #prepare()}, which will retrieve all of the music on the user's device
 * (by performing a query on a content resolver). After that, it's ready to
 * retrieve a random song, with its title and URI, upon request.
 */
public class MusicRetriever {

	public static final String ALL_SONGS = "all.songs";
	public static final String FAVORITES = "favorites";

	final String TAG = "MusicRetriever";

	ContentResolver mContentResolver;

	// the items (songs) we have queried
	// List<Item> mItems = new ArrayList<Item>();
	HashMap<Long, Item> allItems = new HashMap<Long, Item>();
	HashMap<String, PlayList> playLists = new HashMap<String, PlayList>();
	private static MusicRetriever singleton;
	private final AssetManager assetManager;

	public MusicRetriever(ContentResolver cr, AssetManager assetManager) {
		mContentResolver = cr;
		this.assetManager = assetManager;
	}

	public static MusicRetriever getInstance(ContentResolver cr, AssetManager manager) {
		if (singleton == null) {
			singleton = new MusicRetriever(cr, manager);
		}
		return singleton;
	}

	/**
	 * Loads music data. This method may take long, so be sure to call it
	 * asynchronously without blocking the main thread.
	 * 
	 * @throws SAXException
	 * @throws ParserConfigurationException
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public void prepare() throws ParserConfigurationException, SAXException,
			FileNotFoundException, IOException {

		Uri uri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
		Log.i(TAG, "Querying media...");
		Log.i(TAG, "URI: " + uri.toString());

		// Perform a query on the content resolver. The URI we're passing
		// specifies that we
		// want to query for all audio media on external storage (e.g. SD card)
		Cursor cur = mContentResolver.query(uri, null,
				MediaStore.Audio.Media.IS_MUSIC + " = 1", null, null);
		Log.i(TAG, "Query finished. "
				+ (cur == null ? "Returned NULL." : "Returned a cursor."));

		if (cur == null) {
			// Query failed...
			Log.e(TAG, "Failed to retrieve music: cursor is null :-(");
			return;
		}
		if (!cur.moveToFirst()) {
			// Nothing to query. There is no music on the device. How boring.
			Log.e(TAG, "Failed to move cursor to first row (no query results).");
			return;
		}

		Log.i(TAG, "Listing...");

		// retrieve the indices of the columns where the ID, title, etc. of the
		// song are
		int artistColumn = cur.getColumnIndex(MediaStore.Audio.Media.ARTIST);
		int titleColumn = cur.getColumnIndex(MediaStore.Audio.Media.TITLE);
		int albumColumn = cur.getColumnIndex(MediaStore.Audio.Media.ALBUM);
		int durationColumn = cur
				.getColumnIndex(MediaStore.Audio.Media.DURATION);
		int idColumn = cur.getColumnIndex(MediaStore.Audio.Media._ID);

		// cty
		int dataColumn = cur.getColumnIndex(MediaStore.Audio.Media.DATA);
		Log.i(TAG, "Title column index: " + String.valueOf(titleColumn));
		Log.i(TAG, "ID column index: " + String.valueOf(titleColumn));

		PlayList playListAllSongs = new PlayList(ALL_SONGS);

		// add each song to mItems
		do {
			Log.i(TAG,
					"ID: " + cur.getString(idColumn) + " Title: "
							+ cur.getString(titleColumn));
			allItems.put(
					cur.getLong(idColumn),
					new Item(cur.getString(dataColumn), cur.getLong(idColumn),
							cur.getString(artistColumn), cur
									.getString(titleColumn), cur
									.getString(albumColumn), cur
									.getLong(durationColumn)));
			playListAllSongs.addItemId((int) cur.getLong(idColumn));
		} while (cur.moveToNext());

		// overwrite existing one if exists
		playLists.put(playListAllSongs.getName(), playListAllSongs);

		// don't overwrite existing one
		if (playLists.get(FAVORITES) == null) {
			
//			xr.parse(new InputSource(new FileInputStream("favorites.xml")));
			PlayList favorites = new PlayList(FAVORITES);
			favorites.restore();
			playLists.put(FAVORITES, favorites);
		}

		// TODO playlistler arasinda gez, current playlist'in cursor'ini set et.
		PlayList currentPlayList = playLists.get(PlayerState.getInstance()
				.getCurrentPlayList());
		if (currentPlayList != null) {
			currentPlayList.setCurrentId(PlayerState.getInstance()
					.getCurrentId());
		} else {
			PlayerState.getInstance().setCurrentPlayList(ALL_SONGS);
			PlayerState.getInstance().setCurrentId(
					allItems.values().iterator().next().getId());
		}

		Log.i(TAG, "Done querying media. MusicRetriever is ready.");
	}


	public ContentResolver getContentResolver() {
		return mContentResolver;
	}

	/** Returns a random Item. If there are no items available, returns null. */
	public Item getRandomItem(String playListName) {
		PlayList playList = playLists.get(playListName);
		if (playList == null) {
			return null;
		}
		Item item = allItems.get(playList.getRandomItemId());
		return item;
	}

	public static class Item {
		long id;
		String artist;
		String title;
		String album;
		long duration;
		private final String path;

		public Item(String path, long id, String artist, String title,
				String album, long duration) {
			this.path = path;
			this.id = id;
			this.artist = artist;
			this.title = title;
			this.album = album;
			this.duration = duration;
		}

		public long getId() {
			return id;
		}

		public String getArtist() {
			return artist;
		}

		public String getTitle() {
			return title;
		}

		public String getAlbum() {
			return album;
		}

		public long getDuration() {
			return duration;
		}

		public Uri getURI() {
			return ContentUris
					.withAppendedId(
							android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
							id);
		}
	}

	public Collection<Item> getAllItems(String playListName) {
		PlayList playList = playLists.get(playListName);
		if (playList == null) {
			return null;
		}
		Vector<Item> items = new Vector<MusicRetriever.Item>();
		Vector<Long> itemIds = (Vector<Long>) playList.getAllItemIds();
		for (Iterator iterator = itemIds.iterator(); iterator.hasNext();) {
			Long id = (Long) iterator.next();
			items.add(allItems.get(id));
		}
		return items;
	}

	public Item getNextItem(String playListName) {
		PlayList playList = playLists.get(playListName);
		if (playList == null) {
			return null;
		}
		Item item = allItems.get(playList.getNextItemId());
		return item;
	}

	public void createPlayList() {

	}

	public void addToPlayList(String playListName, long id) {
		PlayList playList = playLists.get(playListName);
		if (playList == null) {
			return;
		}
		playList.addItemId(id);
		playList.persist();
	}

	public Item getItemWithId(long id, String currentPlayList) {
		PlayList playList = playLists.get(currentPlayList);
		if (playList == null) {
			return null;
		}
		Item item = allItems.get(id);
		playList.setCurrentId(id);
		return item;

	}

	public Item getCurrentItem(String playListName) {
		PlayList playList = playLists.get(playListName);
		if (playList == null) {
			return null;
		}
		Item item = allItems.get(playList.getCurrentItemId());
		return item;
	}
	
}
