package tr.com.cty;

import com.example.android.musicplayer.MusicRetriever;

public class PlayerState {

	public enum Mode {
		SHUFFLE,
		QUEUE
	}

	private Mode currentMode = Mode.SHUFFLE;
	private String currentPlayList = MusicRetriever.ALL_SONGS;
	private long currentId = 1;
	
	private static PlayerState state = null;
	
	public static PlayerState getInstance() {
		if (state == null) {
			state = new PlayerState();
		}
		return state;
	}

	public Mode getCurrentMode() {
		return currentMode;
	}

	public void setCurrentMode(Mode currentMode) {
		this.currentMode = currentMode;
	}

	public String getCurrentPlayList() {
		return currentPlayList;
	}

	public void setCurrentPlayList(String currentPlayList) {
		this.currentPlayList = currentPlayList;
	}

	public long getCurrentId() {
		return currentId;
	}

	public void setCurrentId(long currentId) {
		this.currentId = currentId;
	}
	

}
