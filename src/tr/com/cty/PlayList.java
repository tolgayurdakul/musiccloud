package tr.com.cty;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xmlpull.v1.XmlSerializer;

import android.os.Environment;
import android.util.Log;
import android.util.Xml;

public class PlayList {

	final String TAG = "PlayList";

	private final String name;

	Random mRandom = new Random();

	Vector<Long> items = new Vector<Long>();
	int cursor = -1;

	private File newxmlfile;

	public PlayList(String name) {
		this.name = name;
		newxmlfile = new File(Environment.getExternalStorageDirectory(),
				getName() + ".xml");

	}

	public void addItemId(long id) {
		items.add(id);
	}

	public void removeItemId(long index) {
		items.removeElement(index);
	}

	public long getNextItemId() {
		if (items.size() <= 0)
			return 0;
		cursor = ++cursor % (items.size());
		if (cursor > items.size() - 1)
			cursor = items.size() - 1;
		long id = items.get(cursor);
		PlayerState.getInstance().setCurrentId(id);
		return id;
	}

	public long getRandomItemId() {
		if (items.size() <= 0)
			return 0;
		// TODO set cursor
		long id = items.get(mRandom.nextInt(items.size()));
		PlayerState.getInstance().setCurrentId(id);
		cursor = items.indexOf(id);
		return id;
	}

	public void resetCursor() {
		cursor = -1;
	}

	public void setCurrentId(long id) {
		PlayerState.getInstance().setCurrentId(id);
		cursor = items.indexOf(id);
	}

	public String getName() {
		return name;
	}

	public int getLength() {
		return items.size();
	}

	public Collection<Long> getAllItemIds() {
		return items;
	}

	public long getCurrentItemId() {
		return items.get(cursor);
	}

	public void persist() {
		try {
			FileOutputStream fileos = new FileOutputStream(newxmlfile);
			XmlSerializer serializer = Xml.newSerializer();
			serializer.setOutput(fileos, "UTF-8");


			serializer.startDocument(null, Boolean.valueOf(true));
			serializer.setFeature(
					"http://xmlpull.org/v1/doc/features.html#indent-output",
					true);
			serializer.startTag(null, "PlayList");
			serializer.attribute(null, "name", getName());
			for (Iterator iterator = items.iterator(); iterator.hasNext();) {
				String id = iterator.next().toString();
				serializer.startTag(null, "Id");
				serializer.text(id);
				serializer.endTag(null, "Id");

			}
			serializer.endTag(null, "PlayList");
			serializer.endDocument();
			serializer.flush();
			fileos.close();
		} catch (Exception e) {
			Log.e("Exception", "Exception occured in writing");
		}
	}

	public boolean restore() {
		try {
			SAXParserFactory spf = SAXParserFactory.newInstance();
			SAXParser sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();
			PlayListPersistenceHelper helper = new PlayListPersistenceHelper(
					this);
			xr.setContentHandler(helper);
			FileInputStream inputStream = new FileInputStream(newxmlfile);
			// InputStream inputStream = assetManager.open("favorites.xml");
			InputSource inputSource = new InputSource(inputStream);
			xr.parse(inputSource);
			return true;
		} catch (Exception e) {
			Log.i(TAG, "Error while parsing file: " + newxmlfile.getName());
		}
		return false;
	}

}
