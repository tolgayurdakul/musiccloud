package tr.com.cty;

import tr.com.cty.musiccloud.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.android.musicplayer.MusicRetriever;
import com.example.android.musicplayer.MusicService;

public class ListActivity extends Activity {

	String[] itemNames;
	long[] itemIds;
	protected ArrayAdapter<String> listAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);
		// getActionBar().setDisplayHomeAsUpEnabled(true);
		final ListView listViewItems = (ListView) findViewById(R.id.listviewitems);
		listViewItems.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> myAdapter, View myView,
					int myItemInt, long mylng) {
				String selectedFromList = (String) (listViewItems
						.getItemAtPosition(myItemInt));
				
				long id = itemIds[myItemInt];
				Intent intentPlaySelected = new Intent(MusicService.ACTION_PLAY_SELECTED);
				intentPlaySelected.putExtra(MusicService.ARGUMENT_SELECTED_ID, id);
				startService(intentPlaySelected);
				
				//return to calling activity.
				finish();
			}
		});
		listViewItems.setLongClickable(true);
		listViewItems.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> myAdapter, View myView,
					int myItemInt, long mylng) {
				String selectedFromList = (String) (listViewItems
						.getItemAtPosition(myItemInt));
				
				long id = itemIds[myItemInt];
				Intent intentPlaySelected = new Intent(MusicService.ACTION_ADD_SELECTED);
				intentPlaySelected.putExtra(MusicService.ARGUMENT_SELECTED_ID, id);
				startService(intentPlaySelected);
				return true;
			}
		});
		

		Intent intentList = new Intent(MusicService.ACTION_LIST);
		intentList.putExtra(MusicService.ARGUMENT_SELECTED_PLAYLIST, PlayerState.getInstance().getCurrentPlayList());
		intentList.putExtra(MusicService.REQUEST_RECEIVER_LIST,
				new ResultReceiver(null) {
					@Override
					protected void onReceiveResult(int resultCode,
							Bundle resultData) {
						itemNames = resultData
								.getStringArray(MusicService.REQUEST_RECEIVER_ITEM_NAMES);
						itemIds = resultData
								.getLongArray(MusicService.REQUEST_RECEIVER_ITEM_IDS);

						// Create ArrayAdapter using the planet list.
						listAdapter = new ArrayAdapter<String>(
								ListActivity.this, R.layout.simplerow,
								itemNames);

						// Set the ArrayAdapter as the ListView's adapter.
						listViewItems.setAdapter(listAdapter);
					}
				});
		startService(intentList);
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.activity_list, menu);
//		return true;
//	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
