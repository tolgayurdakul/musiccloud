package tr.com.cty;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class PlayListPersistenceHelper extends DefaultHandler {

	Boolean currentElement = false;
	String currentValue = "";
	boolean inId = false;
	private final PlayList playList;
	
	public PlayListPersistenceHelper(PlayList playList) {
		this.playList = playList;
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		if (localName.equals("PlayList")) {
//			playList = new PlayList(attributes.getValue("name"));
		} else if (localName.equals("Id")) {
			inId = true;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (localName.equals("Id")) {
			inId = false;
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		String chars = new String(ch, start, length);
		chars = chars.trim();
		if (inId) {
			playList.addItemId(Long.parseLong(chars));			
		}
	}	
}
